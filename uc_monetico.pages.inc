<?php

/**
 * @file
 * Menu callbacks for the uc_monetico module.
 */

/**
 * Form to build the submission to Monetico Web Payment.
 */
function uc_monetico_outgoing_form($form_state, $order) {
  $form = array();

  global $language;
  global $user;
  $fields = array(
    'TPE' => variable_get('uc_monetico_tpe', ''),
    'date' => date('d/m/Y:H:i:s', time()),
    'montant' => round($order->order_total, 2) . variable_get('uc_currency_code', 'USD'),
    'reference' => $order->order_id,
    'texte-libre' => 'userid=' . $user->uid . ' uniqueid=' . md5(uniqid(rand(), TRUE)),
    'mail' => $order->primary_email,
    'lgue' => in_array(strtoupper($language->language), uc_monetico_languages()) ? strtoupper($language->language) : 'EN',
    'societe' => variable_get('uc_monetico_site_code', ''),
    'url_retour_ok' => url('cart/monetico/ok', array('absolute' => TRUE)),
    'url_retour_err' => url('cart/monetico/error', array('absolute' => TRUE)),
  );
  $fields = uc_monetico_complete_request(variable_get('uc_monetico_key', ''), $fields);

  // Add the transaction ID to the order's data array, for extra validation when receiving response.
  $order->data['monetico']['texte-libre'] = $fields['texte-libre'];

  // Save the updated data array to the database.
  uc_monetico_save_order_data($order);

  $form['#action'] = variable_get('uc_monetico_url', UC_MONETICO_TEST_URL);

  foreach ($fields as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit order'),
  );

  return $form;
}

/**
 * Save order's updated data array to the database.
 */
function uc_monetico_save_order_data($order) {
  db_query("UPDATE {uc_orders} SET data = '%s' WHERE order_id = %d", serialize($order->data), $order->order_id);
}

/**
 * Handle an incoming payment.
 */
function uc_monetico_response() {
  if (empty($_POST)) {
    watchdog('uc_monetico', 'Response attempted without any POST data.', array(), WATCHDOG_WARNING);
    return;
  }

  unset($mac_ok);
  $order = uc_monetico_get_order($mac_ok);
  if (!$order) {
    watchdog('uc_monetico', 'Invalid response.', array(), WATCHDOG_ERROR);
    uc_monetico_receipt_exit($mac_ok);
  }

  $order_id = $order->order_id;
  if (($_POST['code-retour'] != 'paiement') && ($_POST['code-retour'] != 'payetest')) {
    uc_order_comment_save($order_id, 0, t("The customer's attempted payment from a bank account may have failed. Authorisation result is @result.", array('@result' => $_POST['motifrefus'])), 'admin');
    uc_monetico_receipt_exit(TRUE);
  }

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'amount',
  );
  $options = array(
    'sign' => FALSE,
  );
  $amount = substr($_POST['montant'], 0, -3);
  $currency = substr($_POST['montant'], -3);

  $comment = t('Monetico Web Payment authorisation #: @number', array('@number' => $_POST['numauto']));
  uc_payment_enter($order_id, 'monetico', $amount, $order->uid, $_POST, $comment);
  $order->data['monetico']['complete_sale_output'] = uc_cart_complete_sale($order);
  uc_monetico_save_order_data($order);

  uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through Monetico Web Payment.', array('@amount' => uc_price($amount, $context, $options), '@currency' => $currency)), 'order', 'payment_received');
  uc_order_comment_save($order_id, 0, t('Monetico Web Payment response reported a payment of @amount @currency.', array('@amount' => uc_price($amount, $context, $options), '@currency' => $currency)));

  uc_monetico_receipt_exit(TRUE);
}

/**
 * Handle a successful payment.
 */
function uc_monetico_return_ok() {
  if (!isset($_SESSION['cart_order'])) {
    drupal_goto('cart');
  }

  // This lets us know it's a legitimate access of the complete page.
  $_SESSION['do_complete'] = TRUE;

  drupal_goto('cart/checkout/complete');
}

/**
 * Handle an unsuccessful payment.
 */
function uc_monetico_return_error() {
  unset($_SESSION['cart_order']);

  drupal_set_message(t('Sorry, your payment could not be processed. Please try again or contact us for assistance.'), 'error');

  drupal_goto('cart');
}

/**
 * Override uc_cart_checkout_complete() when payment method is 'monetico'.
 */
function uc_monetico_checkout_complete() {
  if (!$_SESSION['do_complete']
    || !($order = uc_order_load(intval($_SESSION['cart_order'])))
    || ($order->payment_method != 'monetico')
    || ($order->uid == 0)) {
    require_once(drupal_get_path('module', 'uc_cart') . '/uc_cart.pages.inc');
    return uc_cart_checkout_complete();
  }

  // Get checkout_complete output.
  global $user;
  if ($order->uid == $user->uid) {

    // Build output for the already logged in user.
    $messages['uc_msg_order_submit_format'] = variable_get('uc_msg_order_submit', uc_get_message('completion_message'));
    $messages['uc_msg_order_logged_in_format'] = variable_get('uc_msg_order_logged_in', uc_get_message('completion_logged_in'));
    $messages['uc_msg_continue_shopping_format'] = variable_get('uc_msg_continue_shopping', uc_get_message('continue_shopping'));

    $output_message = '';
    foreach ($messages as $format => $message) {
      $message = token_replace_multiple($message, array('global' => NULL, 'order' => $order));
      $message = check_markup($message, variable_get($format, FILTER_FORMAT_DEFAULT), FALSE);
      $output_message .= $message;
    }
    $output = theme('uc_cart_complete_sale', $output_message);
  }
  else {

    // Use the output built during response.
    $output = $order->data['monetico']['complete_sale_output'];
    unset($order->data['monetico']['complete_sale_output']);
    uc_monetico_save_order_data($order);

    // Login the user if specified.
    if (($user->uid == 0) && variable_get('uc_new_customer_login', FALSE)) {
      user_external_login(user_load($order->uid));
    }
  }

  // Clear cart data.
  uc_cart_empty(uc_cart_get_id());
  unset($_SESSION['cart_order'], $_SESSION['do_complete']);

  // Redirect to a custom page if defined.
  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }

  return $output;
}

/**
 * Return the receipt to the bank then exit cleanly.
 */
function uc_monetico_receipt_exit($mac_ok) {
  print uc_monetico_receipt($mac_ok);
  module_invoke_all('exit');
  exit;
}

function uc_monetico_get_order(&$mac_ok = NULL) {
  $mac_ok = FALSE;
  if (!uc_monetico_validate_response(variable_get('uc_monetico_key', ''), $_POST)) {
    return FALSE;
  }

  $mac_ok = TRUE;
  if (isset($_POST['reference']) && ($order = uc_order_load(intval($_POST['reference'])))) {
    if (isset($order->data['monetico']['texte-libre']) && ($_POST['texte-libre'] == $order->data['monetico']['texte-libre'])) {
      return $order;
    }
  }

  return FALSE;
}
