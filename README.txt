README file for the Monetico Web Payment module


DESCRIPTION
-----------

This module integrates the Monetico Web Payment module with Ubercart.

This is a fork of Ubercart CM-CIC p@iement Credit Card Payment
(https://drupal.org/project/uc_cmcic).

Monetico Web Payment (http://www.partenaires-desjardins.com) is used by
Desjardins (http://www.desjardins.com) and has the same API as CM-CIC p@iement
Credit Card Payment.

Main changes in this fork:

* Moved page callbacks to a separate file.
* Removed unused page callback.
* Modified some labels and messages to use "Monetico Web Payment".
* Removed questionable use of "backup" callback on checkout complete.
* Fixed currency not being properly parsed from the payment response.
* Restricted the list of supported languages to those supported by Monetico Web
  Payment (i.e.: EN, FR).
* Added user id to POST data sent to payment system.

We were initially planning for some more features, but at this point most
changes could probably be ported to Ubercart CM-CIC p@iement Credit Card
Payment. That module could be extended to allow customizing the labels and the
list of allowed languages.


CREDITS
-------

Original code from the Ubercart CM-CIC p@iement Credit Card Payment module
(uc_cmcic):

* Developped by anrikun Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
* http://www.absyx.fr

This fork:

* By Whisky Echo Bravo, http://whiskyechobravo.com.
